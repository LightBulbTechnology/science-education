﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour
{
    public Vector3 offset;
    public Vector3 orbitTarget;
    private Vector3 randomRotation;
    public float movementSpeed;


    private void Start()
    {
        //Move to offset position
        orbitTarget = GameObject.FindGameObjectWithTag("OrbitPoint").transform.position;

        transform.position = orbitTarget;

        offset = new Vector3(0, Random.Range(0.22f, 0.4f), 0);
        transform.position += offset;
        transform.LookAt(orbitTarget);

        randomRotation = new Vector3(Random.Range(-360, 360), 0, Random.Range(-360,360));

        movementSpeed = Random.Range(20, 100);
    }

    private void Update()
    {
        transform.RotateAround(orbitTarget, randomRotation, movementSpeed * Time.deltaTime);
    }
}
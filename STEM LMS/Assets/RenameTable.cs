﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class RenameTable : MonoBehaviour
{
    private string elementsJsonString;

    [SerializeField]
    public ElementList elements;

    // Start is called before the first frame update
    void Start()
    {
        elementsJsonString = File.ReadAllText(Application.dataPath + "/Resources/PeriodicTableOfElementsData.json");
        print(elementsJsonString);
        elements = JsonUtility.FromJson<ElementList>(elementsJsonString);

        foreach(Element element in elements.elementList)
        {
            print("Element: " + element.name + " Appearance: " + element.appearance + " Atomic Mass: " + element.atomic_mass);
        }

        print(elements.elementList.Count);

    }

}

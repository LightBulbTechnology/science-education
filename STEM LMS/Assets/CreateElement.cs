﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateElement : MonoBehaviour
{
    public int protons;
    public int neutrons;
    public int electrons;

    public float atomicNumber;
    public float atomicMass;

    public GameObject ProtonPrefab;
    public GameObject NeutronPrefab;
    public GameObject ElectronPrefab;

    // Start is called before the first frame update
    void Start()
    {
        //Time.timeScale = 0.01f;
        protons = Mathf.RoundToInt(atomicNumber);
        neutrons = Mathf.RoundToInt(atomicMass) - protons;
        electrons = protons;

        print("Protons: " + protons + " Neutrons: " + neutrons + " Electrons: " + electrons);

        SpawnElementComponents(protons, neutrons, electrons);
    }

    private void SpawnElementComponents(int protons, int neutrons, int electrons)
    {
        //Spawn Protons
        for (int i = 0; i <= protons; i++)
        {
            float posX = Random.Range((transform.position.x - 0.5f), (transform.position.x + 0.5f));
            float posY = Random.Range((transform.position.y - 0.5f), (transform.position.y + 0.5f));
            float posZ = Random.Range((transform.position.z - 0.5f), (transform.position.z + 0.5f));

            Vector3 spawnPosition = new Vector3(posX, posY, posZ);
            GameObject.Instantiate(ProtonPrefab, spawnPosition, Quaternion.identity, this.transform);
        }


        //Spawn Neutrons
        for (int i = 0; i <= neutrons; i++)
        {
            float posX = Random.Range((transform.position.x - 0.5f), (transform.position.x + 0.5f));
            float posY = Random.Range((transform.position.y - 0.5f), (transform.position.y +0.5f));
            float posZ = Random.Range((transform.position.z - 0.5f), (transform.position.z + 0.5f));

            Vector3 spawnPosition = new Vector3(posX, posY, posZ);
            GameObject.Instantiate(NeutronPrefab, spawnPosition, Quaternion.identity, this.transform);
        }

        //Spawn Electrons
        for(int i = 0; i <= electrons; i++)
        {
            GameObject.Instantiate(ElectronPrefab, this.transform);
        }

    }
}



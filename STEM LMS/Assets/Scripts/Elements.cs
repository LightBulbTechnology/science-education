﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Element
{
    public string name;
    public string appearance;
    public float atomic_mass;
    public float boil;
    public string category;
    public string color;
    public string density;
    public string discovered_by;
    public float melt;
    public float molar_heat;
    public string named_by;
    public int number;
    public int period;
    public string phase;
    public string source;
    public string spectral_img;
    public string summary;
    public string symbol;
    public string xpos;
    public string ypos;
    public float[] shells;
    public string electron_configuration;
    public float electron_affinity;
    public float electronegativity_pauling;
    public float[] ionization_energies;
}

[System.Serializable]
public class ElementList
{
    public List<Element> elementList;
}
